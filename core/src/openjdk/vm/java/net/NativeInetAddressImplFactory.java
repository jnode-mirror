package java.net;

/**
 * @see java.net.InetAddressImplFactory
 */
class NativeInetAddressImplFactory {
    /**
     * @see java.net.InetAddressImplFactory#isIPv6Supported()
     */
    private static boolean isIPv6Supported() {
        return false;
    }
}
