package org.jnode.shell;

public interface CommandRunnable extends Runnable {
    
    void flushStreams();

    int getRC();

}
